# Laravel Test

Fresh laravel install


1. Using wamp for local server

2. Install composer app in the wamp directory

3. Install latest version of laravel
    1. Open command prompt and run
        cd c:\wamp\www /*Open the wamp directory*/
    2. Run command
        composer create-project laravel/laravel kundan /*It would install laravel in kundan name folder. you can change "kundan" to other folder name*/
    3. cd kundan /*Open project folder*/
    4. composer install --no-scripts /*To generate optimized autoload files*/
    5. composer update /*To check any update and update the composer*/
    6. php artisan serve /*To start Laravel development server , It would create a temporary url to visit the application*/

4. Install Bower 
    npm install -g bower /*Installs the bower into yoour application*/

5. Install Jquery Library
    bower install jquery -save /*Installs latest version of jquery*/
    bower install bootstrap -save /*To add bootstrap with your laravel application*/

6. Middleware 
    php artisan make:middleware CheckAge /*Creates middleware with application*/
    php artisan make:controller MyController -r --model=MyModel /*Creates a model named Mycontroller , type yes to proceed*/

7. authentication system
    Create a database and add it's information in the .env file located on root
    php artisan migrate /*It would migrate with database and will create a user table*/
    php artisan make:auth  /*Will create the login , logout , register , forgot password options. http://127.0.0.1:8000/login for the login page*/

8. Define table hotels
    php artisan make:migration create_hotels_table /*It would create a migration file*/
    Edit in that newly created migration file to create columns in database
    php artisan migrate /*Run this command to migrate this table with database*/

9. Define table Comments
    php artisan make:migration create_comments_table /*It would create a migration file*/
    Edit in that newly created migration file to create columns in database
    php artisan migrate /*Run this command to migrate this table with database*/

10. Route Admin page
    php artisan make:controller Admin /*Create controller for admin page*/
    /*Add these below lines to create route for the admin page*/
    Route::get('/admin', 'Admin@index')->name('admin');
    /*Create controller to access this page only using admin details or send back to a url Admin Details: admin 123456*/
    /*Create a custom form on this and provide a post route to save the form data*/
    Route::post('/admin', 'Admin@create');
    /*Create a controller for saving the hotel data*/

11. Show all hotels list on homepage
    /*Route the main url using controller*/
    Route::get('/', 'HomeController@welcome')->name('welcome');
    /*In controller file use Hotels app and return the hotels details*/
    $hotels = Hotel::get();

12. Hotels Single Page
    /*Create route for hotel single page*/
    Route::get('/hotel', 'HotelController@index')->name('hotel');
    /*fetch hotel details and all comments regarding this hotel and send them to view*/

13. Post comment by logged in User
    /*Send the user type details if is logged in and send them to hotel single page view using controller*/
    /*Open a route to post the comment and create a controller for this*/
    Route::post('/hotel', 'HotelController@create');

14. Seed to create desired user
    php artisan make:seeder UsersTableSeeder /*Create a seeds file inside database/seeds folder*/
    /*Add command to create a user with desired details in the seeds file*/
    php artisan db:seed --class=UsersTableSeeder /*To run the seed and create the desired user*/


